## Body Mass Index Calculator
Simple BMI calculator built using C# and Xamarin Forms

## Results
<p>
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-bmi-calculator/-/raw/master/img1.png" width="200" height="400" />
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-bmi-calculator/-/raw/master/img2.png" width="200" height="400" />
</p>
